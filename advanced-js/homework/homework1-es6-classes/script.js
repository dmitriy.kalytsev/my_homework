// ## Теоретический вопрос
// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
//
// ## Задание
// 1. Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.
// 2. Создайте геттеры и сеттеры для этих свойств.
// 3. Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
// 4. Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
// 5. Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.
//
//     ## Примечание
// Задание должно быть выполнено на "чистом" Javascript без использования библиотек типа jQuery или React.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(name) {
        this._name = name;
    }

    set age(age) {
        this._age = age;
    }

    set salary(salary) {
        this._salary = salary;
    }
}

const firstEmployee = new Employee('David', 24, 1000)
console.log(firstEmployee)

class Programmer extends Employee {
    constructor(name, age, salary, lang,) {
        super(name, age, salary)
        this.lang = lang;
    }

    get salary() {
        return this._salary * 3
    }
}

const firstProgrammer = new Programmer('Arnold', 30, 2000, 'en,rus')
const secondProgrammer = new Programmer('Semen', 26, 1500, 'en,ukr,rus')
console.log(firstProgrammer)
console.log(secondProgrammer)