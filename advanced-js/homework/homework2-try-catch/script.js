const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const div = document.createElement("div");
div.setAttribute('id','#root');
document.body.append(div)

const root = document.getElementById('#root');
const ul = document.createElement('ul');
root.append(ul);

function validateArray() {
    for (const key in books) {
    try {
        if (!books[key].author) {
            throw new Error(books[key].author,'Is not author')
        } else if (!books[key].name) {
            throw new Error('Is not name')
        } else if (!books[key].price) {
            throw new Error('Is not price')
        }
        const li = document.createElement('li');
        li.innerHTML = books[key].author + ' ' + books[key].name + ' ' + books[key].price;
        ul.append(li);
    } catch (error) {
       console.error(error)
    }
}}
validateArray()