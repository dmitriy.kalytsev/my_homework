const url = `https://ajax.test-danit.com/api/swapi/films`
const div = document.querySelector('.root')

function createList(name, characters, episode, opening) {
    return `
        <li>${name}</li>
        <li>${episode}</li>
        <li>${opening}</li>`
}

function createActorList(actorsArray) {
    const actors = actorsArray.map(actor => `<li>${actor.name}</li>`)
    return `<ul>${actors.join('')}</ul>`
}

function createUl() {
    return document.createElement('ul')
}

function appendHtml(htmlElement, list) {
    htmlElement.insertAdjacentHTML('beforeend', list)
}

function appendElement(htmlElement, list) {
    htmlElement.insertAdjacentElement('beforeend', list)
}

const getData = (url) => {
    return fetch(url)
        .then(response => {
            if (response.ok) {
                return response.json()
            } else {
                throw new Error({message: 'error'})
            }
        })
}

async function getActors(urls) {
    let jobs = [];

    urls.forEach(url => {
        let job = fetch(url).then(
            response => {
                if (response.ok) {
                    return response.json()
                } else {
                    return null
                }
            }
        )
        jobs.push(job)
    })
    return await Promise.all(jobs);
}

getData(url)
    .then(result => {
        result.forEach(items => {
            const {name, characters, episodeId, openingCrawl} = items
            const list = createList(name, characters, episodeId, openingCrawl)
            const ul = createUl()
            appendElement(div, ul)
            appendHtml(ul, list)
            getActors(characters).then(result => {
                const actors = createActorList(result)
                appendHtml(ul, actors)
            })
        })
    })