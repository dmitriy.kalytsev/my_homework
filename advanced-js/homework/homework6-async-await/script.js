const root = document.querySelector('.root')

const apiUrl = `https://api.ipify.org/?format=json`
const dataUrl = `http://ip-api.com/json/`

const btn = document.createElement('button')
btn.innerText = 'Знайти по IP'

root.insertAdjacentElement('beforeend', btn)

const getData = async (url) => {
    return await fetch(url)
        .then(response => {
            if (response.ok) {
                return response.json()
            } else {
                throw new Error({message: 'error'})
            }
        })
}

function createList(city, country,regionName,countryCode, zip ){
    return `<ul>
                <li>city: ${city}</li>
                <li>country: ${country}</li>
                <li>regionName: ${regionName}</li>
                <li>countryCode: ${countryCode}</li>
                <li>zip: ${zip}</li>
            </ul>`
}

function appendHtml(htmlElement, list){
    htmlElement.insertAdjacentHTML('beforeend',list)
}

btn.addEventListener('click', () => {
    getData(apiUrl).then(result=>{

        getData(`${dataUrl}` + `${result.ip}`).then(({city, country,regionName,countryCode, zip })=>{
            appendHtml(root, createList(city, country,regionName,countryCode, zip))
        })
    })
})
