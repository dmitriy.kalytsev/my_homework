const urls = ['https://ajax.test-danit.com/api/json/users', 'https://ajax.test-danit.com/api/json/posts']
const deleteUrl = 'https://ajax.test-danit.com/api/json/posts/'

async function getData(urls) {
    let jobs = [];
    urls.forEach(url => {
        let job = fetch(url).then(
            response => {
                if (response.ok) {
                    return response.json()
                } else {
                    return null
                }
            }
        )
        jobs.push(job)
    })
    return await Promise.all(jobs);
}

class Card {
    constructor(userId, body, title, id, user) {
        this.userId = userId
        this.body = body
        this.title = title
        this.id = id
        this.user = user
    }

    deletePost() {
        const post = document.getElementById(this.id)
        post.remove()
        fetch((deleteUrl + `${this.id}`), {
            method: 'DELETE'
        })
            .then(result => console.log(result))
    }

    createViewPost() {
        return `<span class="name">${this.user.name}</span>
                <a href="#">${this.user.email}</a>
                <p class="title">${this.title}</p>
                <p class="text">${this.body}</p>`
    }
}

function render() {
    getData(urls).then(result => {
        const [users, posts] = result
        const cards = []
        posts.forEach(({userId, body, title, id}) => {
            const user = users.find(user => user.id === userId)
            const card = new Card(userId, body, title, id, user)
            cards.push(card)
        })
        cards.forEach(card => {
            const cardView = card.createViewPost(card)
            const div = document.createElement('div')
            div.setAttribute('id', card.id)
            const btn = document.createElement('button')
            btn.addEventListener('click', () => card.deletePost())
            btn.innerText = 'Delete post'
            div.insertAdjacentHTML('beforeend', cardView)
            div.insertAdjacentElement('beforeend', btn)
            document.body.insertAdjacentElement("beforeend", div)
        })
    })
}
render()