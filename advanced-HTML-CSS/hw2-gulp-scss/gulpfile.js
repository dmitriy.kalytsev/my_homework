import gulp from 'gulp'
import concat from 'gulp-concat'
import clean from 'gulp-clean'
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);
import browserSync from 'browser-sync'
import autoprefixer from 'gulp-autoprefixer'
import imagemin from 'gulp-imagemin'
// import minifyjs from 'gulp-js-minify'
// const uglify = require('gulp-uglify')



const path = {
    src: {
        scss: './src/scss/**/*.scss',
        js: './src/js/*.js',
        img: './src/img/*',
        html: './index.html'
    },
    dist: {
        root: './dist/',
        css: './dist/css/',
        js: './dist/js/',
        img: './dist/img/'
    },
}

/** FUNCTION **/

const buildStyles = () => (
    gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false,
        }),)
        .pipe(concat('style.css'))
        // .pipe(uglify())
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream())

);

const buildJs = () => (gulp.src(path.src.js)
    .pipe(concat('script.js'))
    // .pipe(minifyjs())
    // .pipe(uglify())
    .pipe(gulp.dest(path.dist.js))
    .pipe(browserSync.stream()));

const buildImg = () => (gulp.src(path.src.img)
    .pipe(imagemin())
    .pipe(gulp.dest(path.dist.img)));

const devServer = () => (browserSync.init({
    server: {
        baseDir: "./"
    }
}));

const watch = () => {
    devServer();
    gulp.watch(path.src.scss, buildStyles).on('change', browserSync.reload);
    gulp.watch(path.src.js, buildJs).on('change', browserSync.reload);
    gulp.watch(path.src.html).on('change', browserSync.reload);
    gulp.watch(path.src.img, buildImg).on('change', browserSync.reload)
};

const cleanBuild = () => gulp.src(path.dist.root, { allowEmpty: true }).pipe(clean())

/** TASKS **/

const build = gulp.series(buildStyles, buildJs)

gulp.task('dev', gulp.series(cleanBuild, gulp.parallel(buildImg, build), watch))

