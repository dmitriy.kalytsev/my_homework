import {Component} from 'react';
import './modal.scss';


class Modal extends Component {

    render() {
        if (!this.props.isOpen) {
            return null
        }
        return (
            <div className="background" onClick={(e) => this.closeBgd(e)}>
            <div className='modal-main'>
                <h2 className="title-modal">{this.props.title}</h2><span className="closeBtn" onClick={this.onClose}>X</span>
                <p className="description-modal">{this.props.description}</p>
                <div className="btn__confirm">
                <button className="btn__confirm-ok" onClick={this.onClose}>Ok</button>
                <button className="btn__confirm-cancel" onClick={this.onClose}>Cancel</button>
                </div>
            </div>
            </div>
        )
    }
    onClose = () => {
       this.props.onClose()
    }
    closeBgd = (e) => {
        if(e.target.className === "background"){
            this.onClose()
        }
    }
}

export default Modal