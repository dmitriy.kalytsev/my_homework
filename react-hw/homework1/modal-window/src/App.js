import './App.css';
import {Component} from 'react';
import Modal from "./components/Modal/Modal";



class App extends Component {
    modals = [
        {
            id: '1',
            title: "Do you want to delete this file?",
            description: 'Once you have deleted this file,it won\'t be possible to undo this action. Are you sure you want to delete it?',
        },
        {
            id: '2',
            title: "Do you want to create file?",
            description: 'Once you have create file,it won\'t be possible to undo this action. Are you sure you want to create it?'
        }]

    constructor() {
        super();
        this.state = {
            isOpen: false,
            modal: {
                id: 1,
                title: '',
                description: ''
            }
        }
    }

    openModal = (event) => {
        const modalID = event.target.getAttribute('data-modal-id')
        const modal = this.modals.find(item => item.id === modalID)
        this.setState({
            isOpen: true,
            modal
        })
    }

    closeModal = () => {
        this.setState({...this.state, isOpen: false})
    }

    render() {
        return (
            <div className="button__container">
                <button className="button__container-first" data-modal-id="1" onClick={(e) => this.openModal(e)}>Open first modal
                </button>
                <button className="button__container-second" data-modal-id="2" onClick={(e) => this.openModal(e)}>Open second modal
                </button>
                <Modal title={this.state.modal.title}
                       description={this.state.modal.description}
                       onClose={this.closeModal}
                       isOpen={this.state.isOpen}/>
            </div>
        );
    }
}

export default App;
