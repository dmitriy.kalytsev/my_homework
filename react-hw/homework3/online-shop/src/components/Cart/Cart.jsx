import CartIcon from '../../cart.svg'
import {ReactComponent as FavoriteIcon} from '../../favorite.svg'
import PropTypes from "prop-types";


const Cart =({favoritesLength,cartLength})=> {

        return (
            <div className='cart-container'>
                <FavoriteIcon className='favorites__icon'/>
                <span className="favorites__length">{favoritesLength}</span>
                <img src={CartIcon} alt='Cart'/>
                <span className="cart-length">{cartLength}</span>
            </div>
        )

}

Cart.propTypes = {
    cartLength: PropTypes.number,
    favoritesLength: PropTypes.number
}

export default Cart