import Cart from '../Cart/Cart'
import './header.scss';
import PropTypes from "prop-types";
import { Link } from 'react-router-dom'

const Header = ({cartLength, favoritesLength}) => {
    return (
        <div className="header">
            <div className='header__item container'>
                <Link to='/' className='header__title'>Online Shop</Link>
                <div>
                    <Link to='/favorites' className='header__link'>Favorites</Link>
                    <Link to='/cart' className='header__link'>Cart</Link>
                </div>
                <Cart cartLength={cartLength}
                      favoritesLength={favoritesLength}/>
            </div>
        </div>
    )
}

Header.propTypes = {
    cartLength: PropTypes.number,
    favoritesLength: PropTypes.number
}
export default Header