const Button = ({btnClick,backgroundColor,text, classBtn}) => {

        return (
            <button onClick={btnClick}
                    className={classBtn}
                    style={{'backgroundColor': backgroundColor}}>{text}</button>
        )
}

export default Button