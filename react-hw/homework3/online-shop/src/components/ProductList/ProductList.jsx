import {useState, useEffect} from 'react'
import {getProducts} from '../../api/getProducts';
import ProductCard from '../ProductCard/ProductCard'
import './product-list.scss'
import PropTypes from "prop-types";


const ProductList = ({openModal, closeModal, addToFavorites,favorites, deleteFavoriteItem, addToCart}) => {

    const [products, setProducts] = useState([])

    useEffect(() => {
        getProducts().then(result => {
            setProducts(result)
        })
    }, [])

    const productCards = products.map((product) => {

        const active = favorites.some(elem => product.article === elem.article)
        return (<ProductCard key={product.article}
                             isCart={false}
                             active={active}
                             openModal={openModal}
                             closeModal={closeModal}
                             addToCart={addToCart}
                             product={product}
                             deleteFavoriteItem={deleteFavoriteItem}
                             addToFavorites={addToFavorites}
        />)

    })

    return (
        <div className='product-list container'>
            {productCards}
        </div>
    )

}
ProductList.propTypes = {
    addToFavorites: PropTypes.func,
    openModal: PropTypes.func,
    closeModal: PropTypes.func,
}

export default ProductList