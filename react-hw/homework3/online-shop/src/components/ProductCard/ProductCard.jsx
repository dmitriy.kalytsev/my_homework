import './product-card.scss'
import {ReactComponent as Favorite} from '../../favoritefont.svg';
import PropTypes from 'prop-types';
import Button from "../Button/Button";

const ProductCard = ({
                         product: {image, name, price, color, article},
                         addToFavorites,
                         addToCart,
                         product,
                         openModal,
                         closeModal,
                         active,
                         deleteFavoriteItem,
                         isCart,
                         deleteCartItem
                     }) => {


    const toggleActive = () => {
        if (active) {
            deleteFavoriteItem(article)
        } else {
            addToFavorites(product)
        }
    }

    const openModalDeleteFromCard = () => {
        openModal(product, {
            header: 'Do you want delete from cart?',
            actions: <>
                <Button classBtn='button' btnClick={()=>{deleteCartItem(article)}} text='Delete' backgroundColor={'grey'}/>
                <Button btnClick={closeModal} text='Cancel' backgroundColor={'grey'}/>
            </>
        })
    }

    const openModalAddToCart = () => {
        openModal(product, {
            header: 'Do you want add to cart?',
            actions: <>
                <Button classBtn='button' btnClick={()=>addToCart(product)} text='Add' backgroundColor={'grey'}/>
                <Button btnClick={closeModal} text='Cancel' backgroundColor={'grey'}/>
            </>
        })
    }

    return (
        <div className='product__card'>
            <ul className='product__card-item'>
                {isCart ? <span onClick={() => openModalDeleteFromCard()}>X</span> : null}
                <img className='product__card-item-img' src={image} alt={name}/>
                <li>Name: {name}</li>
                <li>Price:{price}</li>
                <li>Color:{color}</li>
                <li>Article:{article}</li>
                <div className='buttons'>
                    <button className='buttons__add'
                            onClick={() => openModalAddToCart()}>
                        ADD TO CART
                    </button>
                    <Favorite className={active ? 'favorite-active' : 'favorite'} onClick={() => toggleActive()}/>
                </div>
            </ul>
        </div>
    )
}

ProductCard.propTypes = {
    product: PropTypes.object,
    addToFavorites: PropTypes.func,
    openModal: PropTypes.func,
    closeModal: PropTypes.func,
}

export default ProductCard
