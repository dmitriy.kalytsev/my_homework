import './modal.scss';
import PropTypes from "prop-types";

const Modal = ({isOpen, closeButton, header, closeModal, text, actions}) => {


    if (!isOpen) {
        return null
    }
    const closeStyle = {'display': closeButton ? 'block' : 'none'}

    const onClose = () => {
        closeModal()
    }

    const closeBgd = (e) => {
        if (e.target.className === "background") {
            onClose()
        }
    }
    return (
        <div className="background" onClick={(e) => closeBgd(e)}>
            <div className='modal-main'>
                <h2 className="title-modal">{header}</h2>
                <span style={closeStyle} className="close-btn" onClick={onClose}>X</span>
                <div className="description-modal">{text}</div>
                <div className="btn__confirm">{actions}</div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    actions: PropTypes.string,
    closeButton: PropTypes.bool,
    closeModal: PropTypes.func,
    header: PropTypes.string,
    isOpen: PropTypes.bool,
}

export default Modal