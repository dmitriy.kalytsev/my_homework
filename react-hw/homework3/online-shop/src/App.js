import './App.css';
import {useEffect, useState} from 'react';
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import {AppRoutes} from "./routes/AppRoutes";

const App = () => {
    const [cart, setCart] = useState([])
    const [favorites, setFavorites] = useState([])
    const [isOpen, setIsOpen] = useState(false)
    const [modal, setModal] = useState({
        id: 1,
        header: '',
        closeButton: true,
        text: '',
        actions: '',
        data: {}
    })


    const openModal = (product, modalData) => {
        const text = <>
            <p>Name: {product.name}</p>
            <p>Article: {product.article}</p>
            <p>Price: {product.price}</p>
            <p>Color: {product.color}</p>
        </>
        setModal({
            data: product,
            text,
            header: modalData.header,
            actions: modalData.actions
        })
        setIsOpen(true)

    }

    const closeModal = () => {
        setIsOpen(false)
    }

    const addToCart = (product) => {
        const isCart = cart.some((item) => item.article === product.article)
        if (isCart) {
            closeModal()
            return alert('Cart is added')

        }
        const newCart = [...cart].concat(product)
        // const  newCart.concat(modal.data)
        setCart(newCart)
        setIsOpen(false)
        // console.log(cart, '50')
        localStorage.setItem('cart', JSON.stringify(newCart))
    }
    const addToFavorites = (product) => {
        const isFavorite = favorites.some((item) => item.article === product.article)
        if (isFavorite) return;
        const newFavorites = [...favorites].concat(product)
        setFavorites(newFavorites)
        // console.log(newFavorites, 'new')
        localStorage.setItem('favorites', JSON.stringify(newFavorites))
    }
    const deleteFavoriteItem = (article) => {
        const newFavorites = favorites.filter(item => item.article !== article)
        setFavorites(newFavorites)
        localStorage.setItem('favorites', JSON.stringify(newFavorites))
    }
    const deleteCartItem = (article) => {
        const newCart = cart.filter(item => item.article !== article)
        setCart(newCart)
        closeModal()
        localStorage.setItem('cart', JSON.stringify(newCart))
    }
    useEffect(() => {
        const lsFavorites = JSON.parse(localStorage.getItem('favorites'))
        const lsCart = JSON.parse(localStorage.getItem('cart'))
        setFavorites(lsFavorites ? lsFavorites : [])
        setCart(lsCart ? lsCart : [])

    }, [])


    return (
        <>
            <Modal
                header={modal.header}
                text={modal.text}
                closeButton={true}
                closeModal={closeModal}
                isOpen={isOpen}
                actions={modal.actions}

            />
            <Header cartLength={cart.length ? cart.length : null}
                    favoritesLength={favorites.length ? favorites.length : null}

            />
            <AppRoutes cart={cart}
                       favorites={favorites}
                       openModal={openModal}
                       closeModal={closeModal}
                       addToCart={addToCart}
                       deleteCartItem={deleteCartItem}
                       addToFavorites={addToFavorites}
                       deleteFavoriteItem={deleteFavoriteItem}

            />
        </>
    );

}


export default App;
