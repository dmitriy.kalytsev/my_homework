import ProductCard from "../../components/ProductCard/ProductCard";

export const Favorites = ({favorites, openModal, closeModal, addToFavorites, deleteFavoriteItem, addToCart}) => {
    const productCards = favorites.map((favorite) => {

        return (<ProductCard key={favorite.article}
                             isCart={false}
                             active={true}
                             openModal={openModal}
                             closeModal={closeModal}
                             product={favorite}
                             addToCart={addToCart}
                             addToFavorites={addToFavorites}
                             deleteFavoriteItem={deleteFavoriteItem}
        />)

    })
    return (
        <>
            <h1 className='container'>Favorites</h1>
            <div className='product-list container'>
                {productCards}
            </div>
        </>
    )
}