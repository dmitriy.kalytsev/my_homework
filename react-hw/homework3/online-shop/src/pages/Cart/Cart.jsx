import ProductCard from "../../components/ProductCard/ProductCard";

export const Cart = ({carts, openModal, closeModal, addToFavorites, deleteFavoriteItem, favorites, deleteCartItem}) => {

    const productCards = carts.map((cart) => {
        const active = favorites.some(elem => cart.article === elem.article)
        return (<ProductCard
            active={active}
            isCart={true}
            openModal={openModal}
            closeModal={closeModal}
            product={cart}
            deleteCartItem={deleteCartItem}
            addToFavorites={addToFavorites}
            deleteFavoriteItem={deleteFavoriteItem}
        />)

    })
    return (
        <>
            <h1 className='container'>Cart</h1>
            <div className='product-list container'>

                {productCards}
            </div>
        </>

    )


}