import {Route, Routes} from "react-router-dom";
import ProductList from "../components/ProductList/ProductList";
import {Favorites} from "../pages/Favorites/Favorites";
import {Cart} from "../pages/Cart/Cart";

export const AppRoutes = ({
                              cart,
                              favorites,
                              openModal,
                              closeModal,
                              addToFavorites,
                              deleteFavoriteItem,
                              addToCart,
                              deleteCartItem
                          }) => {
    return (
        <Routes>
            <Route path="/favorites" element={
                <Favorites
                    favorites={favorites}
                    deleteFavoriteItem={deleteFavoriteItem}
                    addToCart={addToCart}
                    openModal={openModal}
                    closeModal={closeModal}
                />}>
            </Route>
            <Route path="/cart" element={
                <Cart carts={cart}
                      openModal={openModal}
                      closeModal={closeModal}
                      favorites={favorites}
                      deleteCartItem={deleteCartItem}
                      addToFavorites={addToFavorites}
                      deleteFavoriteItem={deleteFavoriteItem}/>}>
            </Route>
            <Route path="/" element={
                <ProductList favorites={favorites}
                             openModal={openModal}
                             closeModal={closeModal}
                             addToCart={addToCart}
                             addToFavorites={addToFavorites}
                             deleteFavoriteItem={deleteFavoriteItem}
                />}
            />
        </Routes>
    )
}