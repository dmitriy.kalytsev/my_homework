import './App.css';
import {Component} from 'react';
import Modal from "./components/Modal/Modal";
import ProductList from "./components/ProductList/ProductList";
import Header from "./components/Header/Header";
import Button from "./components/Button/Button";


class App extends Component {

    constructor() {
        super();
        this.state = {
            cart: [],
            favorites: [],
            isOpen: false,
            modal: {
                id: 1,
                header: '',
                closeButton: true,
                text: '',
                actions: '',
                data: {}
            }
        }
    }

    openModal = (product) => {
        const text = <>
            <p>Name: {product.name}</p>
            <p>Article: {product.article}</p>
            <p>Price: {product.price}</p>
            <p>Color: {product.color}</p>
        </>

        this.setState({
            ...this.state,
            modal: {
                data: product,
                text,
                header: 'Do you want to add an item to your shopping cart?'
            },
            isOpen: true,
        })
    }

    closeModal = () => {
        this.setState({...this.state, isOpen: false})
    }

    addToCart = () => {
        const cart = [...this.state.cart]
        const newCart = cart.concat([this.state.modal.data])
        this.setState({
            ...this.state,
            isOpen: false,
            cart: newCart
        })
        localStorage.setItem('cart', JSON.stringify(newCart))
    }

    componentDidMount() {
        const cart = JSON.parse(localStorage.getItem('cart'))
        const favorites = JSON.parse(localStorage.getItem('favorites'))
        this.setState({
            ...this.state,
            cart: cart ? cart : [],
            favorites: favorites ? favorites : [],
        })
    }


    addToFavorites = (product) => {
        const favorites = [...this.state.favorites]
        const isFavorite = favorites.some((card) => card.article === product.article)
        if (isFavorite) return this.state;
        const newFavorites = favorites.concat([product])
        this.setState({
            ...this.state,
            favorites: newFavorites
        })
        localStorage.setItem('favorites', JSON.stringify(newFavorites))
    }


    render() {
        return (
            <>
                <Modal
                    header={this.state.modal.header}
                    text={this.state.modal.text}
                    closeButton={true}
                    closeModal={this.closeModal}
                    isOpen={this.state.isOpen}
                    actions={<>
                        <Button btnClick={this.addToCart} text='Ok' backgroundColor={'grey'}/>
                        <Button btnClick={this.closeModal} text='Cancel' backgroundColor={'grey'}/>
                    </>
                    }/>
                <Header cartLength={this.state.cart.length ? this.state.cart.length : null}
                        favoritesLength={this.state.favorites.length ? this.state.favorites.length : null}/>
                <ProductList openModal={this.openModal} closeModal={this.closeModal}
                             addToFavorites={this.addToFavorites}/>

            </>
        );
    }
}


export default App;
