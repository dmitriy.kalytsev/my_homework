import {Component} from 'react';
import './modal.scss';
import PropTypes from "prop-types";

class Modal extends Component {

    render() {
        if (!this.props.isOpen) {
            return null
        }
        const closeStyle = {'display': this.props.closeButton ? 'block' : 'none'}
        return (
            <div className="background" onClick={(e) => this.closeBgd(e)}>
                <div className='modal-main'>
                    <h2 className="title-modal">{this.props.header}</h2>
                    <span style={closeStyle} className="close-btn" onClick={this.onClose}>X</span>
                    <div className="description-modal">{this.props.text}</div>
                    <div className="btn__confirm">{this.props.actions}</div>
                </div>
            </div>
        )
    }

    onClose = () => {
        this.props.closeModal()
    }
    closeBgd = (e) => {
        if (e.target.className === "background") {
            this.onClose()
        }
    }
}

Modal.propTypes = {
    actions: PropTypes.object,
    closeButton: PropTypes.bool,
    closeModal: PropTypes.func,
    header: PropTypes.string,
    isOpen: PropTypes.bool,
}

export default Modal