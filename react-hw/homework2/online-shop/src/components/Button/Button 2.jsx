import {Component} from 'react'

class Button extends Component {

    render() {
        return (
            <button className='' onClick={this.props.btnClick}
                    style={{'backgroundColor': this.props.backgroundColor}}>{this.props.text}</button>
        )
    }
}

export default Button