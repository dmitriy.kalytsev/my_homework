import {Component} from 'react';
import './product-card.scss'
import {ReactComponent as Favorite} from '../../favoritefont.svg';
import PropTypes from 'prop-types';

class ProductCard extends Component {

    addToFavorites = () => {
        this.props.addToFavorites(this.props.product)
    }

    render() {

        const {image, name, price, color, article} = this.props.product
        return (
            <div className='product__card'>
                <ul key={article} className='product__card-item'>
                    <img className='product__card-item-img' src={image} alt={name}/>
                    <li>Name: {name}</li>
                    <li>Price:{price}</li>
                    <li>Color:{color}</li>
                    <li>Article:{article}</li>
                    <div className='buttons'>
                        <button className='buttons__add'
                                onClick={(e) => this.props.openModal(this.props.product)}>
                            ADD TO CART
                        </button>
                        <Favorite className={this.props.active ? 'favorite-active' : 'favorite'} onClick={(event) => {
                            event.target.classList.add('favorite-active')
                            this.addToFavorites()
                        }}/>
                    </div>
                </ul>
            </div>
        )
    }
}

ProductCard.propTypes = {
    product: PropTypes.object,
    addToFavorites: PropTypes.func,
    openModal: PropTypes.func,
    closeModal: PropTypes.func,
}

export default ProductCard
