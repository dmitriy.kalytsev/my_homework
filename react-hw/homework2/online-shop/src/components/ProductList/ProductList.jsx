import {Component} from 'react'
import {getProducts} from '../../api/getProducts';
import ProductCard from '../ProductCard/ProductCard'
import './product-list.scss'
import PropTypes from "prop-types";

class ProductList extends Component {
    favorites = []

    constructor() {
        super();
        this.state = {
            products: [],
        }
    }


    componentDidMount() {
        getProducts().then(result => {
            this.setState({products: result})
        })
        this.favorites = JSON.parse(localStorage.getItem('favorites')) ? JSON.parse(localStorage.getItem('favorites')) : []

    }


    render() {
        const productCards = this.state.products.map((item) => {
            const active = this.favorites.some(elem => item.article === elem.article)
            return (<ProductCard key={item.article}
                                 openModal={this.props.openModal}
                                 closeModal={this.props.closeModal}
                                 product={item}
                                 active={active}
                                 addToFavorites={this.props.addToFavorites}/>)
        })

        return (
            <div className='product-list container'>
                {productCards}
            </div>
        )
    }
}

ProductList.propTypes = {
    addToFavorites: PropTypes.func,
    openModal: PropTypes.func,
    closeModal: PropTypes.func,
}

export default ProductList