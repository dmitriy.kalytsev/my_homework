import {Component} from 'react'
import CartIcon from '../../cart.svg'
import {ReactComponent as FavoriteIcon} from '../../favorite.svg'
import PropTypes from "prop-types";


class Cart extends Component {

    render() {
        return (
            <div className='cart-container'>
                <FavoriteIcon className='favorites__icon'/>
                <span className="favorites__length">{this.props.favoritesLength}</span>
                <img src={CartIcon} alt='Cart'/>
                <span className="cart-length">{this.props.cartLength}</span>
            </div>
        )
    }
}

Cart.propTypes = {
    cartLength: PropTypes.number,
    favoritesLength: PropTypes.number
}

export default Cart