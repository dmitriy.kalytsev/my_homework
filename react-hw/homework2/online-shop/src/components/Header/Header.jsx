import {Component} from 'react'
import Cart from '../Cart/Cart'
import './header.scss';
import PropTypes from "prop-types";

class Header extends Component {

    render() {

        return (
            <div className="header">
                <div className='header__item container'>
                    <h1 className='header__title'>Online Shop</h1>
                    <Cart cartLength={this.props.cartLength}
                          favoritesLength={this.props.favoritesLength}/>
                </div>
            </div>

        )
    }
}

Header.propTypes = {
    cartLength: PropTypes.number,
    favoritesLength: PropTypes.number
}
export default Header