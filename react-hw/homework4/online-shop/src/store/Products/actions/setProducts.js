import {SET_PRODUCTS} from "../constants";

export const setProducts = (payload) => {
    return {
        type: SET_PRODUCTS,
        payload
    }
}