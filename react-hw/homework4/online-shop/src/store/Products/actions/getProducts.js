import {GET_PRODUCTS} from "../constants";

export const getProducts = (payload) => {
    return {
        type: GET_PRODUCTS,
        payload
    }
}