import {GET_PRODUCTS, SET_PRODUCTS} from "./constants";

export const productReducer = (state = {products: []}, {type,payload}) => {
    switch (type) {
        case GET_PRODUCTS:
            return {...state, products: payload}
        case SET_PRODUCTS:
            return {...state, products: payload}
        default:
            return state
    }
}