export const ADD_TO_FAVORITES = 'ADD_TO_FAVORITES'
export const ADD_TO_FAVORITES_FROM_LS = 'ADD_TO_FAVORITES_FROM_LS'
export const DELETE_FROM_FAVORITES = 'DELETE_FROM_FAVORITES'