import {DELETE_FROM_FAVORITES} from "../constants";

export const deleteFromFavorites = (payload) => {
    return{
        type: DELETE_FROM_FAVORITES,
        payload
    }
}