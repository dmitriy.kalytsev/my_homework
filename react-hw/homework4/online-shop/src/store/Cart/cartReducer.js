import {ADD_TO_CART_FROM_LS, ADD_TO_CART, DELETE_FROM_CART} from "./constants";

export const cartReducer = (state = {cart: []}, {type, payload}) => {
    switch (type) {
        case ADD_TO_CART:
            const newAddedCart = [...state.cart, payload]
            localStorage.setItem('cart', JSON.stringify(newAddedCart))
            return {...state, cart: newAddedCart}
        case ADD_TO_CART_FROM_LS:
            return {...state, cart: payload}
        case DELETE_FROM_CART:
            const newDeletedCart = [...state.cart].filter(item => item.article !== payload)
            localStorage.setItem('cart', JSON.stringify(newDeletedCart))
            return {...state, cart: newDeletedCart};
        default:
            return state
    }
}