import {DELETE_FROM_CART} from "../constants";

export const deleteFromCart = (payload)=>{
    return {
        type: DELETE_FROM_CART,
        payload
    }
}