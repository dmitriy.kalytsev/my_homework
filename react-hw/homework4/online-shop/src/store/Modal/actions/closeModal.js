import {CLOSE_MODAL} from "../constants";

export const closeModal = ()=>{
    return {
        type: CLOSE_MODAL
    }
}