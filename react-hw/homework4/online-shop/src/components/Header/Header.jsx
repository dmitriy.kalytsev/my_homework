import Cart from '../Cart/Cart'
import './header.scss';
import {Link} from 'react-router-dom'

const Header = () => {

    return (
        <div className="header">
            <div className='header__item container'>
                <Link to='/' className='header__title'>Online Shop</Link>
                <div>
                    <Link to='/favorites' className='header__link'>Favorites</Link>
                    <Link to='/cart' className='header__link'>Cart</Link>
                </div>
                <Cart/>
            </div>
        </div>
    )
}

export default Header