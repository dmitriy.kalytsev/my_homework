import './modal.scss';
import {useDispatch, useSelector} from "react-redux";
import {closeModal} from "../../store/Modal/actions/closeModal";

const Modal = () => {
    const dispatch = useDispatch()
    const state = useSelector(state => state.modal)
    const isOpen = state.isOpen
    const style = {
        display : isOpen ? 'flex': 'none'
    }
    const closeStyle = {'display': state.closeButton ? 'block' : 'none'}
    const onClose = () => {
      dispatch(closeModal())
    }

    const closeBgd = (e) => {
        if (e.target.className === "background") {
            onClose()
        }
    }
    return (
        <div style={style} className="background" onClick={(e) => closeBgd(e)}>
            <div className='modal-main'>
                <h2 className="title-modal">{state.header}</h2>
                <span style={closeStyle} className="close-btn" onClick={onClose}>X</span>
                <div className="description-modal">{state.text}</div>
                <div className="btn__confirm">{state.actions}</div>
            </div>
        </div>
    )
}

export default Modal