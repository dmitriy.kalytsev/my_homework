import './App.css';
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import {AppRoutes} from "./routes/AppRoutes";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {addToCartFromLS} from "./store/Cart/actions/addToCartFromLS";
import {addToFavoritesFromLS} from "./store/Favorites/actions/addToFavoritesFromLS";

const App = () => {

    const dispatch = useDispatch()
    useEffect(() => {
        const lsFavorites = JSON.parse(localStorage.getItem('favorites')) ? JSON.parse(localStorage.getItem('favorites')) : []
        const lsCart = JSON.parse(localStorage.getItem('cart')) ? JSON.parse(localStorage.getItem('cart')) : []
        dispatch(addToCartFromLS(lsCart))
        dispatch(addToFavoritesFromLS(lsFavorites))

    }, [])


    return (
        <>
            <Modal/>
            <Header/>
            <AppRoutes/>
        </>

    );
}


export default App;
