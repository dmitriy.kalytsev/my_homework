import {Route, Routes} from "react-router-dom";

import ProductList from "../components/ProductList/ProductList";
import {Favorites} from "../pages/Favorites/Favorites";
import {Cart} from "../pages/Cart/Cart";
import CheckoutForm from "../components/CheckoutForm";

export const AppRoutes = () => {
    return (
        <Routes>
            <Route path="/favorites" element={<Favorites/>}/>
            <Route path="/cart" element={<><Cart/><CheckoutForm/></>}/>
            <Route path="/" element={<ProductList/>}/>

        </Routes>
    )
}