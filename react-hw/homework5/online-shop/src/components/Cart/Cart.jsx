import CartIcon from '../../cart.svg'
import {ReactComponent as FavoriteIcon} from '../../favorite.svg'
import {useSelector} from "react-redux";


const Cart = () => {
    const cartState = useSelector(state => state.cart)
    const favoritesState = useSelector(state => state.favorites)
    const cartLength = cartState.cart.length ? cartState.cart.length : null
    const favoritesLength = favoritesState.favorites.length ? favoritesState.favorites.length : null

    return (
        <div className='cart-container'>
            <FavoriteIcon className='favorites__icon'/>
            <span className="favorites__length">{favoritesLength}</span>
            <img src={CartIcon} alt='Cart'/>
            <span className="cart-length">{cartLength}</span>
        </div>
    )

}

export default Cart