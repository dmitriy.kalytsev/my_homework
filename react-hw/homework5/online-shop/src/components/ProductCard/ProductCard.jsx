import './product-card.scss'
import {ReactComponent as Favorite} from '../../favoritefont.svg';
import PropTypes from 'prop-types';
import Button from "../Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {openModal} from "../../store/Modal/actions/openModal";
import {addToCart} from "../../store/Cart/actions/addToCart";
import {closeModal} from "../../store/Modal/actions/closeModal";
import {addToFavorites} from "../../store/Favorites/actions/addToFavorites";
import {deleteFromFavorites} from "../../store/Favorites/actions/deleteFromFavorites";
import {deleteFromCart} from "../../store/Cart/actions/deleteFromCart";

const ProductCard = ({product, active, isCart}) => {
    // const cartsState = useSelector(state => state.cart)
    const dispatch = useDispatch()
    const toggleActive = () => {
        if (active) {
            dispatch(deleteFromFavorites(product.article))
        } else {
            dispatch(addToFavorites(product))
        }
    }

    const confirmAdded = () => {
        // const hasInCart = cartsState.cart.some(cart => cart.article !== product.article);
        dispatch(addToCart(product))
        dispatch(closeModal())
    }
    const confirmDeleted = () => {
        dispatch(deleteFromCart(product.article))
        dispatch(closeModal())
    }

    const openModalAddToCart = () => {
        const text = <>
            <p>Name: {product.name}</p>
            <p>Article: {product.article}</p>
            <p>Price: {product.price}</p>
            <p>Color: {product.color}</p>
        </>
        dispatch(openModal({
            header: 'Do you want to add to cart?',
            product: product,
            text,
            actions: <>
                <Button btnClick={confirmAdded} text='Add'/>
                <Button text='Cancel' btnClick={() => dispatch(closeModal())}/>
            </>
        }));
    }
    const deleteFromCarts = () => {
        const text = <>
            <p>Name: {product.name}</p>
            <p>Article: {product.article}</p>
            <p>Price: {product.price}</p>
            <p>Color: {product.color}</p>
        </>
        dispatch(openModal({
            header: 'Do you want to delete from cart?',
            product: product,
            text,
            actions: <>
                <Button btnClick={confirmDeleted} text='Delete'/>
                <Button text='Cancel' btnClick={() => dispatch(closeModal())}/>
            </>
        }));
    }

    return (
        <div className='product__card'>
            <ul className='product__card-item'>
                {isCart ? <span onClick={deleteFromCarts}>X</span> : null}
                <img className='product__card-item-img' src={product.image} alt={product.name}/>
                <li>Name: {product.name}</li>
                <li>Price:{product.price}</li>
                <li>Color:{product.color}</li>
                <li>Article:{product.article}</li>
                <div className='buttons'>
                    <button className='buttons__add'
                            onClick={() => openModalAddToCart()}>
                        ADD TO CART
                    </button>
                    <Favorite className={active ? 'favorite-active' : 'favorite'} onClick={() => toggleActive()}/>
                </div>
            </ul>
        </div>
    )
}

ProductCard.propTypes = {
    product: PropTypes.object,
}

export default ProductCard
