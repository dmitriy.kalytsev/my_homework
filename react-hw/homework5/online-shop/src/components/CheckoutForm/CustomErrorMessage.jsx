import {ErrorMessage} from 'formik'

export const CustomErrorMessage = ({name}) => (
    <ErrorMessage name={name}>
        {message => (
            <div className='error'>
                <i>{message}</i>
            </div>
        )}
    </ErrorMessage>
)
