import {Formik, Field, Form, replace} from 'formik'
import {useDispatch, useSelector} from 'react-redux'
import './checkout.scss'
import {deleteCartFromLS} from '../../store/Cart/actions/deleteCartFromLS'
import {object, string, number} from 'yup'
import {CustomErrorMessage} from './CustomErrorMessage'
import {useNavigate} from 'react-router-dom';
import {deleteFromCartAll} from '../../store/Cart/actions/deleteFromCartAll'



const CheckoutForm = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate();
    const cartState = useSelector(state => state.cart)

    const handleSubmit = (values) => {
        dispatch(deleteCartFromLS())
        dispatch(deleteFromCartAll())
        navigate('/',{replace:true});
        console.log('products', cartState.cart)
        console.log('Delivery', values)





    }
    const checkoutSchema = object({
        firstName: string().required('FirstName is required'),
        lastName: string().required('LastName is required'),
        age: number().required("Age is required"),
        address: string().required('Address is required'),
        phone: number().required("Phone number is required")
    })

    return (
        <Formik initialValues={{
            firstName: '',
            lastName: '',
            age: '',
            address: '',
            phone: ''
        }}
                onSubmit={handleSubmit}
                validationSchema={checkoutSchema}
        >

            {propsFormik => (

                <div className='container '>
                    <Form className="checkout__form">
                        <h3>Fill out the purchase form</h3>
                        <div className='checkout__form-item'>
                            <label className='label' htmlFor="firstName">First Name</label>
                            <Field className='checkout__form-input' id='firstName' name="firstName"/>
                            <CustomErrorMessage name="firstName"/>
                        </div>
                        <div className='checkout__form-item'>
                            <label className='label' htmlFor="lastName">Last Name</label>
                            <Field onBlur={propsFormik.handleBlur} className='checkout__form-input' id='lastName'
                                   name="lastName"/>
                            <CustomErrorMessage name="lastName"/>
                        </div>
                        <div className='checkout__form-item'>
                            <label className='label' htmlFor="age">Age</label>
                            <Field className='checkout__form-input' id='age' name="age"/>
                            <CustomErrorMessage name="age"/>
                        </div>
                        <div className='checkout__form-item'>
                            <label className='label' htmlFor="address">Delivery address</label>
                            <Field className='checkout__form-input' id='address' name="address"/>
                            <CustomErrorMessage name="address"/>
                        </div>
                        <div className='checkout__form-item'>
                            <label className='label' htmlFor="phone">Phone number</label>
                            <Field className='checkout__form-input' id='phone' name="phone"/>
                            <CustomErrorMessage name="phone"/>
                        </div>
                        <button className='checkout__btn' type='submit'>Checkout</button>
                    </Form>
                </div>
            )}

        </Formik>
    )
}
export default CheckoutForm