import {useEffect} from 'react'
import {getAllProducts} from '../../api/getProducts';
import ProductCard from '../ProductCard/ProductCard'
import './product-list.scss'
import {useDispatch, useSelector} from "react-redux";
import {setProducts} from "../../store/Products/actions/setProducts";


const ProductList = () => {
    const dispatch = useDispatch()
    const favoritesState = useSelector(state => state.favorites)
    useEffect(() => {
        getAllProducts().then(result => {
            dispatch(setProducts(result))
        })
    }, [])
    const productState = useSelector(state => state.products)
    const productCards = productState.products.map((product) => {
        const active = favoritesState.favorites.some(elem => product.article === elem.article)
        return (<ProductCard key={product.article}
                             isCart={false}
                             active={active}
                             product={product}
        />)
    })
    return (
        <div className='product-list container'>
            {productCards}
        </div>
    )
}

export default ProductList