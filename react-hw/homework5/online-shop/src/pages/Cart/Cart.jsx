import ProductCard from "../../components/ProductCard/ProductCard";
import {useSelector} from "react-redux";

export const Cart = () => {
    const cartsState = useSelector(state => state.cart)
    const favoritesState = useSelector(state => state.favorites)
    const productCards = cartsState.cart.map((cart) => {
        const active = favoritesState.favorites.some(elem => cart.article === elem.article)
        return (<ProductCard key={cart.article}
                             active={active}
                             isCart={true}
                             product={cart}
        />)
    })
    return (
        <>
            <h1 className='container'>Carts</h1>
            <div className='product-list container'>
                {productCards}
            </div>
        </>
    )
}