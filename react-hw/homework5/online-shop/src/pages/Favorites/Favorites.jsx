import ProductCard from "../../components/ProductCard/ProductCard";
import {useSelector} from "react-redux";

export const Favorites = () => {
    const favoritesState = useSelector(state => state.favorites)
    const productCards = favoritesState.favorites.map((favorite) => {
        return (<ProductCard key={favorite.article}
                             isCart={false}
                             active={true}
                             product={favorite}
        />)
    })
    return (
        <>
            <h1 className='container'>Favorites</h1>
            <div className='product-list container'>
                {productCards}
            </div>
        </>
    )
}