import {ADD_TO_CART} from "../constants";

export const addToCart = (payload)=>{
    return {
        type: ADD_TO_CART,
        payload

    }
}