import {DELETE_FROM_CART_ALL} from "../constants";

export const deleteFromCartAll = (payload)=>{
    return {
        type: DELETE_FROM_CART_ALL,
        payload
    }
}