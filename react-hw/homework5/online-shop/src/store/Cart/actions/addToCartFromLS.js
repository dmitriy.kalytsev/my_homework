import {ADD_TO_CART_FROM_LS} from "../constants";

export const addToCartFromLS = (payload) => {
    return {
        type: ADD_TO_CART_FROM_LS,
        payload
    }
}