import {DELETE_CART_FROM_LS} from "../constants";

export const deleteCartFromLS = (payload) => {
    return {
        type: DELETE_CART_FROM_LS,
        payload
    }
}