import {ADD_TO_FAVORITES, ADD_TO_FAVORITES_FROM_LS, DELETE_FROM_FAVORITES} from "./constants";

export const favoritesReducer = (state = {favorites: []},{type,payload}) => {
    switch(type) {
        case ADD_TO_FAVORITES:
            const newAddedFavorites = [...state.favorites, payload]
            localStorage.setItem('favorites', JSON.stringify(newAddedFavorites))
            return {...state, favorites: newAddedFavorites};
        case ADD_TO_FAVORITES_FROM_LS:
            return {...state,favorites:payload}
        case DELETE_FROM_FAVORITES:
            const newDeletedFavorites = [...state.favorites].filter(item=>item.article !== payload)
            localStorage.setItem('favorites', JSON.stringify(newDeletedFavorites))
            return {...state, favorites: newDeletedFavorites};
        default: return state
    }
}