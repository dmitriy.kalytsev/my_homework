import {ADD_TO_FAVORITES} from "../constants";

export const addToFavorites = (payload) => {
    return {
        type: ADD_TO_FAVORITES,
        payload
    }

}