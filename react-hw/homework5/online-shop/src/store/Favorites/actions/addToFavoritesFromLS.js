import {ADD_TO_FAVORITES_FROM_LS} from "../constants";

export const addToFavoritesFromLS = (payload) => {
    return {
        type: ADD_TO_FAVORITES_FROM_LS,
        payload
    }
}