import {CLOSE_MODAL, OPEN_MODAL} from "./constants";
import {initialState} from './initialState';

export const modalReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case OPEN_MODAL:
            return {...state, isOpen: true, text: payload.text, actions: payload.actions, header: payload.header};
        case CLOSE_MODAL :
            return {...state, isOpen: false, data: {}}
        default:
            return state
    }
}