import {createStore, combineReducers, applyMiddleware} from 'redux'
import {composeWithDevTools} from "@redux-devtools/extension";
import thunk from "redux-thunk";
import {modalReducer} from "./Modal/modalReducer";
import {cartReducer} from "./Cart/cartReducer";
import {favoritesReducer} from "./Favorites/favoritesReducer";
import {productReducer} from "./Products/productReducer";

const rootReducer = combineReducers({
    modal: modalReducer,
    cart: cartReducer,
    favorites: favoritesReducer,
    products: productReducer
})

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))

export default store