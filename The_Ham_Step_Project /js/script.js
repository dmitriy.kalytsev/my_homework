// tabs

const tabs = document.querySelectorAll('.tabs-title')
tabs.forEach((tab, index) => {
  tab.addEventListener('click', function () {

    this.closest('.tabs').querySelector('.active').classList.remove('active');
    this.classList.add('active');

    const tabsContent = document.querySelector('.tabs-content')
    tabsContent.querySelector('.active').classList.remove('active');


    const listContent = tabsContent.querySelectorAll('li')
    listContent[index].classList.add('active');
  })
})


// amazing work

document.getElementById("showmore").addEventListener("click", () => {
  const allHiddenImgs = document.querySelectorAll(".hidden")
  allHiddenImgs.forEach(singleImg => {
    singleImg.classList.remove("hidden")
    const activeTab = document.querySelector(".amazing-tabs-title.active")
    const attr = activeTab.dataset.content
    const itemAttr = singleImg.dataset.content
    if (itemAttr === attr || attr === "All") {
      singleImg.classList.add("show")
    }
    const button = document.querySelector(".load-more-btn")
    button.style.display = "none"
  })
})
const tabsFilter = document.querySelectorAll('.amazing-tabs-title');
const wrapCard = document.querySelectorAll('.amazing-gallery-card');

tabsFilter.forEach(tab => {
  tab.addEventListener("click",(event) => {
    const activeTab = document.querySelector(".amazing-tabs-title.active")
    activeTab.classList.remove("active")
    event.currentTarget.classList.add("active")
   const attr = event.currentTarget.dataset.content
   wrapCard.forEach(card => {
     const itemAttr = card.dataset.content
     card.classList.remove("show")
     if (itemAttr === attr || attr === "All") {
       card.classList.add("show")
     }
   })
  })
})

// Slider

let currentSlide = 0;
const navigation = document.querySelectorAll('.slider-user-foto-small');
const slides = document.querySelectorAll('.slider-user-review');
const next = document.getElementById('arrowRight');
const previous = document.getElementById('arrowLeft');

for(let i = 0; i < navigation.length; i++) {
  navigation[i].onclick = function () {
    currentSlide = i;
    document.querySelector('.slider-user-review.active').classList.remove('active');
    document.querySelector('.slider-user-foto-small.active').classList.remove('active');
    navigation[currentSlide].classList.add('active');
    slides[currentSlide].classList.add('active');
  }
}

next.onclick = function() {
  nextSlide(currentSlide);
};

previous.onclick = function() {
  previousSlide(currentSlide);
};

function nextSlide() {
  goToSlide(currentSlide+1);
}

function previousSlide() {
  goToSlide(currentSlide-1);
}

function goToSlide(n){
  hideSlides();
  currentSlide = (n+slides.length)%slides.length;
  showSlides();
}

function hideSlides(){
  slides[currentSlide].className = 'slider-user-review container';
  navigation[currentSlide].className = 'slider-user-foto-small';
}

function showSlides(){
  slides[currentSlide].className = 'slider-user-review container active';
  navigation[currentSlide].className = 'slider-user-foto-small active';
}


