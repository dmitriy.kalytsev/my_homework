// Обработчик событий может после действия пользователя на сайте запустить функция которая выполнится.

const input = document.createElement('input')
input.placeholder = 'Price'
document.body.append(input)

input.addEventListener("focus", function () {
    input.style.border = '2px solid green';
    input.placeholder = ""

})

input.addEventListener("blur", function () {
    const hasStatusText = document.querySelector('span')
    if (hasStatusText) {
        hasStatusText.remove()
    }
    input.style.border = '1px solid black';
    const statusText = document.createElement('span')
    if (input.value < 0) {

        statusText.textContent = `Please enter correct price`
        document.body.insertAdjacentElement('afterend', statusText)
        input.style.border = '2px solid red'

    } else if (input.value > 0) {
        const button = document.createElement('button')
        button.textContent = 'X'
        statusText.textContent = `Текущая цена:  ${input.value}`
        input.style.color = 'green'
        document.body.insertAdjacentElement('beforebegin', statusText)
        statusText.insertAdjacentElement('beforeend', button)

        button.addEventListener('click', function () {
            input.value = ''
            statusText.remove()
            input.style.color = 'black'
        })
    }
})

