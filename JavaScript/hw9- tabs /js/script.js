
const tabs = document.querySelectorAll('.tabs-title')
tabs.forEach((tab, index) => {
    tab.addEventListener('click', function () {

        this.closest('.tabs').querySelector('.active').classList.remove('active');
        this.classList.add('active');

        const tabsContent = document.querySelector('.tabs-content')
       tabsContent.querySelector('.active').classList.remove('active');


        const listContent = tabsContent.querySelectorAll('li')
        listContent[index].classList.add('active');

    })
})