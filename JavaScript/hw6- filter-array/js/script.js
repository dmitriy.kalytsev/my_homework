// 1.Метод forEach перебирает масив .

function filterBy(array, dataType) {
    if (Array.isArray(array)) {
        const newArray = array.filter(element => (typeof element) !== dataType);
        console.log(newArray);
    } else {
        alert('Error')
    }
}

const arr = ['Елемент масива', 'hello', 'world', 23, '23', null];
const filter = prompt("Введите тип данных который хотите отфильтровать:", "undefined, number, bigint, boolean, string, symbol, object, function")
filterBy(arr, filter);
