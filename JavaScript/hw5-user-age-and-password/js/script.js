// 1.Экранирование помогает использовать специальные символы как обычные, для этого используем "/" , "\".

function createNewUser(firstName, lastName, birthday) {
    return {
        name: firstName,
        lastName: lastName,
        userBirthday: birthday,

        getLogin() {
            return this.name[0].toLowerCase() + this.lastName.toLowerCase();
        },

        getAge() {
            const today = new Date();
            const userDay = Date.parse(`${this.userBirthday.slice(6)}-${this.userBirthday.slice(3, 5)}-${this.userBirthday.slice(0, 2)}`);
            const userAge = ((today - userDay) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed();
            if (userAge < today) {
                return `Вам ${userAge - 1} лет`;
            } else {
                return `Вам ${userAge} лет`;
            }
        },

        getPassword() {
            return this.name[0].toUpperCase() + this.lastName.toLowerCase() + this.userBirthday.slice(-4);
        },
    }
}

const newUser = createNewUser(prompt("Введите Ваше имя"), prompt("Введите Вашу фамилию"), prompt("Введите дату Вашего рождения", "текст в формате dd.mm.yyyy"))
console.log(newUser.getLogin())
console.log(newUser.getAge());
console.log(newUser.getPassword());





