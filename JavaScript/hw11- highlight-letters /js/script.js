// Почему для работы с input не рекомендуется использовать события клавиатуры?
// Потому что событие для клавиатуры в поле input может получить данные не только с клавиатурного ввода , вставка может быть с помощью мыши или распознавание речи при диктовке текста.


const allKey = document.querySelectorAll('.btn')

document.addEventListener('keydown', function (event){
allKey.forEach(button => {
    if(event.key === button.textContent) {
        const active = document.querySelector('.active')
        if(active){
            active.classList.remove('active');
        }
        button.classList.add('active');

    }
})
})